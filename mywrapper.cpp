
//#include "mywrapper.h"

#ifdef MYWRAPPER


namespace myproj
{

    //BASE CONSTRUCTOR -- SETS SOME SCENE SETTINGS AND INITIALIZES COLLISION VECTOR
    template <typename T, int n>
    MyWrapper<T, n>::MyWrapper() : GMlib::PSphere<T>()
    {
        this->toggleDefaultVisualizer();
        this->replot(5,5,0,0);
        this->setVisible(false);
        this->collObjs = std::vector<Coll<T>>();
    }

    //COLLISION "CONTROLLER" FUNCTION
    template <typename T, int n>
    void MyWrapper<T, n>::coll(double dt)
    {       
        //CALCULATE IF COLLISION WILL OCCUR BETWEEN BALLS AND WALLS WITHIN DT
        for (int i = 0 ; i < this->planets.size(); i++)
        {
            for (int j = i+1 ; j < this->planets.size(); j++)
            {
                Coll<T> newColl = this->computeCollBallBall(this->planets[i], this->planets[j], 0);
                if (newColl.isInit())
                {
                    this->collObjs.push_back(newColl);
                }
            }
            for (int k = 0 ; k < this->walls.size(); k++)
            {
                Coll<T> newColl = this->computeCollBallWall(this->planets[i], this->walls[k], 0);
                if (newColl.isInit())
                {
                    this->collObjs.push_back(newColl);
                }
            }
        }

        //WHILE THERE IS A COLLISION WITHIN THIS DT
        while (this->collObjs.size() > 0)
        {
            //SORT/UNIQUE THE COLLISIONS
            std::sort(this->collObjs.begin(), this->collObjs.end());
            std::unique(this->collObjs.begin(), this->collObjs.end());

            //POP FRONT COLLISION, THIS IS THE FIRST ONE HANDLED
            Coll<T> workingColl = this->collObjs[0];
            this->collObjs.erase(collObjs.begin());

            //HANDLE THE COLLISION (UPDATE STEPS AND VELOCITIES
            workingColl.handle(dt);

            //FIND NEW COLLISIONS AFTER HANDLING THE FIRST (BETWEEN X AND DT) AND REPEAT

            //BALL/WALL COLLISION
            if(workingColl.isBallWall())
            {
                //SETTING THE STEP DT
                workingColl.getBall1()->setXDS(workingColl.getDt());

                //FIND NEW BALL/BALL COLLISIONS AFTER THIS ONE HAS BEEN HANDLED
                for (int i = 0; i < this->planets.size(); i++)
                {
                    if (workingColl.getBall1() != this->planets[i])
                    {
                        Coll<T> newColl = this->computeCollBallBall(workingColl.getBall1(), this->planets[i], workingColl.getDt());
                        if (newColl.isInit())
                        {
                            this->collObjs.push_back(newColl);
                        }
                    }
                }

                //FIND NEW BALL/WALL COLLISIONS AFTER THIS ONE HAS BEEN HANDLED
                for (int i = 0; i < this->walls.size(); i++)
                {
                    if (workingColl.getWall() != this->walls[i])
                    {
                        Coll<T> newColl = this->computeCollBallWall(workingColl.getBall1(), this->walls[i], workingColl.getDt());
                        if (newColl.isInit())
                        {
                            this->collObjs.push_back(newColl);
                        }
                    }
                }
            }

            //BALL/BALL COLLISIONS
            else
            {

                //SETTING THE STEP DT
                workingColl.getBall1()->setXDS(workingColl.getDt());
                workingColl.getBall2()->setXDS(workingColl.getDt());

                //FIND NEW BALL/BALL COLLISIONS AFTER THIS ONE HAS BEEN HANDLED
                for (int i = 0; i < this->planets.size(); i++)
                {
                    if (workingColl.getBall1() != this->planets[i])
                    {
                        Coll<T> newColl = this->computeCollBallBall(workingColl.getBall1(), this->planets[i], workingColl.getDt());
                        if (newColl.isInit())
                        {
                               this->collObjs.push_back(newColl);
                        }
                    }
                    if (workingColl.getBall2() != this->planets[i])
                    {
                         Coll<T> newColl = this->computeCollBallBall(workingColl.getBall2(), this->planets[i], workingColl.getDt());
                         if (newColl.isInit())
                         {
                                this->collObjs.push_back(newColl);
                         }
                    }
                }

                //FIND NEW BALL/WALL COLLISIONS AFTER THIS ONE HAS BEEN HANDLED
                for (int i = 0; i < this->walls.size(); i++)
                {
                    Coll<T> newColl1 = this->computeCollBallWall(workingColl.getBall1(), this->walls[i], workingColl.getDt());
                    if (newColl1.isInit())
                    {
                           this->collObjs.push_back(newColl1);
                    }

                    Coll<T> newColl2 = this->computeCollBallWall(workingColl.getBall2(), this->walls[i], workingColl.getDt());
                    if (newColl2.isInit())
                    {
                           this->collObjs.push_back(newColl2);
                    }
                }

             }



          }


       }

    //FUNCTION TO COMPUTE IF A COLLISION WILL OCCUR BETWEEN 2 BALLS
    template <typename T, int n>
    Coll<T> MyWrapper<T,n>::computeCollBallBall(myproj::Ball<T>* obj1, myproj::Ball<T>* obj2, double dt)
    {
        Coll<T> returnColl;
        GMlib::Vector<T, n> q = obj1->getCenterPos() - obj2->getCenterPos();
        GMlib::Vector<T, n> ds = obj1->getDS() - obj2->getDS();
        T r = obj1->getRadius() + obj2->getRadius();

        T sqrt = ( std::pow(q * ds, 2) ) - ( (ds * ds) * ( (q * q) - (std::pow(r,2)) ) );

        if (sqrt > 0 && (ds*ds) > 0)
        {
             T x = ( ( (-1) * (q * ds) ) - std::sqrt( sqrt ) ) / (ds * ds);

             if (dt < x && x <= 1)
             {
                 returnColl = Coll<T>(obj1, obj2, x);
                 return returnColl;
             }
        }

        returnColl = Coll<T>();
        return returnColl;

    }


    //FUNCTION TO COMPUTE IF A COLLISION WILL OCCUR BETWEEN A BALL AND A WALL
    template <typename T, int n>
    Coll<T> MyWrapper<T,n>::computeCollBallWall(myproj::Ball<T>* obj1, myproj::Plane<T>* obj2, double dt)
    {
        Coll<T> returnColl;

        GMlib::Point<T, n> d = obj1->getPos();
        GMlib::Vector<T, n> ds = obj1->getDS();

        obj2->getClosestPoint(this->getPos(), this->_u, this->_v);
        GMlib::DMatrix<GMlib::Vector<T,n>> wallMatrix = obj2->evaluate(this->_u, this->_v, 1,1);

        GMlib::Vector<T,n> dV = wallMatrix[0][0] - d;

        GMlib::Vector<T,n> normal = obj2->getNormal();
        auto a=ds*normal;
        auto b=dV*normal;
        auto r=obj1->getRadius();

        if (a != 0)
        {
            T x = (r+b)/a;

            if (dt < x && x <= 1)
            {
                returnColl = Coll<T>(obj1, obj2, dt);
                return returnColl;
            }
        }

        returnColl = Coll<T>();
        return returnColl;
    }


    //LOCAL SIMULATE
    template <typename T, int n>
    void MyWrapper<T, n>::localSimulate(double dt)
    {
        //UPDATE VECTOR STEPS
        this->updateStep(dt);
        //HANDLE COLLISIONS
        this->coll(dt);
    }

    //UPDATE THE STEP(DS) OF EACH BALL
    template <typename T, int n>
    void MyWrapper<T, n>::updateStep(double dt)
    {
        for (int i = 0; i < planets.size(); i++)
        {
             planets[i]->updateStep(dt);
        }
    }

    //FUNCTION TO INSERT BALLS INTO THE CONTROLLER (USES RANDOM VELOCITIES AND TRANSLATIONS)
    template <typename T, int n>
    void MyWrapper<T, n>::insertBall(myproj::Ball<T>* newBall)
    {
        T randVelX = GMlib::Random<T>(-1.0f, 1.0f).get() * 10.0f;
        T randVelY = GMlib::Random<T>(-1.0f, 1.0f).get() * 10.0f;
        T randVelZ = GMlib::Random<T>(-1.0f, 1.0f).get() * 0.0f;
        T randMass = GMlib::Random<T>(-1.0f, 1.0f).get() * 2.0f;
        T randTransX = GMlib::Random<T>(0.0f, 1.0f).get() * 10.0f;
        T randTransY = GMlib::Random<T>(0.0f, 1.0f).get() * 10.0f;

        newBall->setFloor(this->floor);
        newBall->setVelocity(GMlib::Vector<T,3>(randVelX, randVelY, randVelZ));
        newBall->setMass(randMass);
        newBall->setMaterial(GMlib::GMmaterial::PolishedBronze);
        this->insert(newBall);
        this->planets.push_back(newBall);
        newBall->translate(GMlib::Vector<T,n>(randTransX, randTransY, newBall->getRadius()));
    }

    //FUNCTION TO INSERT WALLS INTO THE CONTROLLER
    template <typename T, int n>
    void MyWrapper<T, n>::insertWall(myproj::Plane<T>* newWall)
    {
        newWall->toggleDefaultVisualizer();
        newWall->setMaterial(GMlib::GMmaterial::BlackPlastic);
        newWall->replot(30,30,1,1);
        this->insert(newWall);
        this->walls.push_back(newWall);
    }

    //FUNCTION TO INSERT A FLOOR INTO THE CONTROLLER
    template <typename T, int n>
    void MyWrapper<T, n>::insertFloor(GMlib::PSurf<T,n>* newFloor, GMlib::DMatrix<GMlib::Vector<T,n>> floorMatrix)
    {
        this->pointmat = floorMatrix;
        this->floor = newFloor;
        this->insert(newFloor);
    }


}

#endif
