#ifndef BALL
#define BALL

#include <parametrics/gmpsphere>
#include <parametrics/gmpsurf>
#include <core/gmdvector>

namespace myproj
{

template <typename T>
class Ball : public GMlib::PSphere<T>
{
    public:
     using GMlib::PSphere<T>::PSphere;

     void collisionSimulate(double dt);

     void updateStep(double dt);

     void setMass(T m);
     void setVelocity(GMlib::Vector<T, 3> v);
     void setFloor(GMlib::PSurf<T,3>* floor);
     void setDS(GMlib::Vector<T,3> newDS);
     void setRotationVector(GMlib::UnitVector<T,3> newRotation);
     void setXDS(T newXDS);

     T getMass();
     GMlib::Vector<T, 3> getVelocity();
     GMlib::PSurf<T,3>* getFloor();
     GMlib::Vector<T,3> getDS();
     GMlib::UnitVector<T,3> getRotationVector();
     T getXDS();
     T getU();
     T getV();

    protected:
     void localSimulate(double dt) override {

         GMlib::Vector<T,3> step = (1-xDS)*ds;
         this->translateParent(step);
         this->rotateParent(GMlib::Angle(step.getLength()/this->getRadius()),this->rotation); //rotation of the ball
         xDS=0;

  }

    private:
     GMlib::Vector<T, 3> velocity {0.0f,0.0f,0.0f};
     T mass {5.0f};
     GMlib::UnitVector<T,3> rotation;
     GMlib::Vector<T,3> g {0.0f,0.0f,-9.81f};
     GMlib::UnitVector<T,3> n;

     GMlib::Vector<T,3> ds;
     T xDS;

     float _u;
     float _v;

     GMlib::PSurf<T,3>* floor;
};

}

#include "ball.cpp"


#endif // BALL

