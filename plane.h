#ifndef PLANE
#define PLANE

#include <parametrics/gmpplane>

namespace myproj
{

template <typename T>
class Plane : public GMlib::PPlane<T>
{
    public:
     using GMlib::PPlane<T>::PPlane;

    void setMass(T m);
    void setVelocity(GMlib::Vector<T, 3> v);

    T getMass();
    GMlib::Vector<T, 3> getVelocity();

    private:
     GMlib::Vector<T, 3> velocity { GMlib::Vector<float,3>( 0.0f, 0.0f, 0.0f ) };
     T mass {5.0f};


};

}

#include "plane.cpp"


#endif // PLANE

