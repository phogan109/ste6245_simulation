#ifndef BEZIERSURFACE
#define BEZIERSURFACE

#include <parametrics/gmpbeziersurf>

namespace myproj
{

template <typename T>
class BSurface : public GMlib::PBezierSurf<T>
{
    GM_SCENEOBJECT(BSurface)
    public:
     using GMlib::PBezierSurf<T>::PBezierSurf;

    protected:
     void localSimulate(double dt) override {

         //this->translate( dt * velocity );
         //this->rotate( dt * rotation, velocity  );

  }


    private:
     bool m_test01 {false};


};

}



#endif // BEZIERSURFACE

