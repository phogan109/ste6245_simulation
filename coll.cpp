//#include "coll.h"

#ifdef COLL_H

namespace myproj
{
    //DEFAULT CONSTRUCTOR - USED TO CREATE BLANK COLLISIONS
    template <typename T>
    Coll<T>::Coll()
    {
        this->init = 0;
    }

    //BALL BALL COLLISION CONSTRUCTOR
    template <typename T>
    Coll<T>::Coll(Ball<T>* ball1, Ball<T>* ball2, double dt)
    {
        this->ball1 = ball1;
        this->ball2 = ball2;
        this->dt = dt;
        this->init = 1;
    }

    //BALL WALL COLLISION CONSTRUCTOR
    template <typename T>
    Coll<T>::Coll(Ball<T>* ball1, Plane<T>* wall, double dt)
    {
        this->ball1 = ball1;
        this->wall = wall;
        this->dt = dt;
        this->ballWall = 1;
        this->init = 1;
    }

    //COLLISION DESTRUCTOR
    template <typename T>
    Coll<T>::~Coll()
    {

    }

    //HANDLE COLLISION - GETS CALLED WHEN COLLISION OBJECT IS BEING HANDLED
    template <typename T>
    void Coll<T>::handle(double x)
    {
        //SET X PROPORTIONAL TO DT IF X IS DIFFERENT THAN 1
        x = x * this->getDt();

        //IF BALL WALL COLLISION
        if (this->ballWall)
        {
            GMlib::Vector<float,3> nV = this->ball1->getVelocity();
            nV -= 2 * (this->ball1->getVelocity() * this->wall->getNormal() ) * this->wall->getNormal();
            this->ball1->setVelocity(nV);
            this->ball1->updateStep(x);
        }
        //ELSE BALL BALL COLLISION
        else
        {
            GMlib::Vector<T, 3> nVBall1;
            GMlib::Vector<T, 3> nVBall2;

            GMlib::UnitVector<T, 3> d = this->ball2->getPos() - this->ball1->getPos();

            T dd =d*d;

            GMlib::Vector<T, 3> v1 = ((this->ball1->getVelocity() * d)/dd)*d;
            GMlib::Vector<T, 3> v1n = this->ball1->getVelocity() - v1;

            GMlib::Vector<T, 3> v2 = ((this->ball2->getVelocity() * d)/dd)*d;
            GMlib::Vector<T, 3> v2n = this->ball2->getVelocity() - v2;

            T m1 = this->ball1->getMass();
            T m2 = this->ball2->getMass();

            GMlib::Vector<T, 3> _v1 = ((m1 - m2)/(m1 + m2))*v1 + ((2*m2)/(m1 + m2))*v2;
            GMlib::Vector<T, 3> _v2 = ((m2 - m1)/(m1 + m2))*v2 + ((2*m1)/(m1 + m2))*v1;

            nVBall1 = _v1 + v1n;
            nVBall2 = _v2 + v2n;

            //SET NEW VELOCITIES FOR THE BALLS
            this->ball1->setVelocity(nVBall1);
            this->ball2->setVelocity(nVBall2);
            //SET NEW DS FOR THE BALLS
            this->ball1->updateStep(x);
            this->ball2->updateStep(x);
        }

    }

    //FUNCTION RETURNS TRUE IF COLLISION IS BALL WALL
    template <typename T>
    bool Coll<T>::isBallWall() const
    {
        return this->ballWall;
    }

    //FUNCTION RETURNS TRUE IF COLLISION OBJECT IS NOT EMPTY
    template <typename T>
    bool Coll<T>::isInit()
    {
        return this->init;
    }

    //GETTER FOR DT
    template <typename T>
    double Coll<T>::getDt() const
    {
        return this->dt;
    }

    //GETTER FOR BALL1
    template <typename T>
    Ball<T>* Coll<T>::getBall1() const
    {
        return this->ball1;
    }

    //GETTER FOR BALL2
    template <typename T>
    Ball<T>* Coll<T>::getBall2() const
    {
        return this->ball2;
    }

    //GETTER FOR WALL
    template <typename T>
    Plane<T>* Coll<T>::getWall() const
    {
        return this->wall;
    }

    //< OPERATOR OVERLOAD (USED IN SORT AND UNIQUE)
    template <typename T>
    Coll<T>::operator < (const Coll& coll)
    {
        return this->getDt() < coll.getDt();
    }

    //== OPERATOR OVERLOAD (USED IN SORT AND UNIQUE)
    template <typename T>
    Coll<T>::operator == (const Coll& coll)
    {
        if (this->getBall1() == coll.getBall1()) return true;
        if (!coll.isBallWall() && this->getBall1() == coll.getBall2()) return true;
        if (!this->isBallWall() && this->getBall2() == this->getBall1()) return true;
        if (!this->isBallWall() && !coll.isBallWall() && this->getBall2() == coll.getBall2()) return true;
        return false;
    }



}

#endif

