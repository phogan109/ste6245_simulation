#ifndef MYWRAPPER
#define MYWRAPPER

#include "coll.h"
#include "asteroid.h"
#include "ball.h"
#include "plane.h"
#include <parametrics/gmpbeziersurf>
#include <typeinfo>
#include <gmCoreModule>
#include <gmSceneModule>

namespace myproj
{

template <typename T, int n>
class MyWrapper : public GMlib::PSphere<T>
{
    GM_SCENEOBJECT(MyWrapper)
public:
    MyWrapper();

    void insertBall(myproj::Ball<T>*);
    void insertWall(myproj::Plane<T>*);
    void insertFloor(GMlib::PSurf<T,n>*, GMlib::DMatrix<GMlib::Vector<T,n>>);

    void updateStep(double dt);
    void coll(double dt);

    Coll<T> computeCollBallBall(myproj::Ball<T>* obj1, myproj::Ball<T>* obj2, double dt);
    Coll<T> computeCollBallWall(myproj::Ball<T>* obj1, myproj::Plane<T>* obj2, double dt);

    std::vector<Coll<T>> getCollObjs();

protected:
    void localSimulate(double dt);

private:

    std::vector<Coll<T>> collObjs;
    std::vector<Ball<T>*> planets;
    std::vector<Plane<T>*> walls;
    GMlib::PSurf<T, n>* floor;
    GMlib::DMatrix<GMlib::Vector<T, n>> pointmat {n,n};
    constexpr static long double gravity {0.00000000006674};

    T sizex {20.0f};
    T sizey {20.0f};
    T sizez {10.0f};
    GMlib::Vector<T,n> midControl {sizex/2, sizey/2, sizez};
    bool dir {0};

};

}

#include "mywrapper.cpp"






#endif // MYWRAPPER

