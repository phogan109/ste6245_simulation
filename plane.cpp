
#ifdef PLANE

namespace myproj
{
    //PLACE HOLDER FOR MASS SETTER
    template <typename T>
    void Plane<T>::setMass(T m)
    {
        this->mass = m;
    }

    //PLACE HOLDER FOR VELOCITY SETTER
    template <typename T>
    void Plane<T>::setVelocity(GMlib::Vector<T, 3> v)
    {
        this->velocity = v;
    }

    //PLACE HOLDER FOR MASS GETTER
    template <typename T>
    T Plane<T>::getMass()
    {
        return this->mass;
    }

    //PLACE HOLDER FOR VELOCITY GETTER
    template <typename T>
    GMlib::Vector<T, 3> Plane<T>::getVelocity()
    {
        return this->velocity;
    }
}


#endif
