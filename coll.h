#ifndef COLL_H
#define COLL_H

#include "ball.h"
#include "plane.h"

namespace myproj
{

    template <typename T>
    class Coll
    {
    public:
        Coll();
        Coll(Ball<T>*, Ball<T>*, double dt);
        Coll(Ball<T>*, Plane<T>*, double dt);
        ~Coll();

        void handle(double x);

        bool isBallWall() const;
        bool isInit();

        double getDt() const;
        Ball<T>* getBall1() const;
        Ball<T>* getBall2() const;
        Plane<T>* getWall() const;

        operator < (const Coll&);
        operator == (const Coll&);

    private:
        Ball<T>* ball1;
        Ball<T>* ball2;
        Plane<T>* wall;
        double dt;

        bool init {0};
        bool ballWall {0};


    };



}

#include "coll.cpp"

#endif // COLL_H
