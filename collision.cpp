//#include "collision.h"


#ifdef COLLISION

namespace myproj
{

/***************************DEFUNCT **********************************/

        template <typename T, typename N>
        Collision<T,N>::Collision()
        {

        }

        template <typename T, typename N>
        Collision<T,N>::Collision(T* obj1, N* obj2, double dt)
        {
            this->obj1 = obj1;
            this->obj2 = obj2;
            this->dt = dt;
            this->init = true;
        }

        template <typename T, typename N>
        Collision<T,N>::~Collision()
        {

        }

        template <typename T, typename N>
        Collision<T,N>::operator < (const Collision& coll)
        {
            return this->getDt() < coll.getDt();
        }

        template <typename T, typename N>
        Collision<T,N>::operator == (const Collision<T,N>& coll)
        {
            if (this->obj1 == coll.getObj1()) return true;
            if (this->ballBall() && this->obj1 == coll.getObj2()) return true;
            if (this->ballBall() && this->obj2 == coll.getObj1()) return true;
            if (this->ballBall() && coll.ballBall() && this->obj2 == coll.getObj2()) return true;
            return false;
        }

        template <typename T, typename N>
        T* Collision<T,N>::getObj1() const
        {
            return obj1;
        }

        template <typename T, typename N>
        N* Collision<T,N>::getObj2() const
        {
            return obj2;
        }

        template <typename T, typename N>
        double Collision<T,N>::getDt() const
        {
            return dt;
        }

        template <typename T, typename N>
        bool Collision<T,N>::getInit() const
        {
            return init;
        }

        template <typename T, typename N>
        void Collision<T,N>::localSimulateObj1()
        {
            myproj::Ball<float>* ballPoint = dynamic_cast<myproj::Ball<float>*>(obj1);
            ballPoint->collisionSimulate(this->dt);
        }

        template <typename T, typename N>
        void Collision<T,N>::localSimulateObj2()
        {
            myproj::Ball<float>* ballPoint = dynamic_cast<myproj::Ball<float>*>(obj2);
            ballPoint->collisionSimulate(this->dt);
        }

        template <typename T, typename N>
        void Collision<T,N>::setVelocityObj1(GMlib::Vector<float,3> newVel)
        {
            this->newVelocityObj1 = newVel;
        }

        template <typename T, typename N>
        void Collision<T,N>::setVelocityObj2(GMlib::Vector<float,3> newVel)
        {
            this->newVelocityObj2 = newVel;
        }

        template <typename T, typename N>
        void Collision<T,N>::passVelocityObj1()
        {
            myproj::Ball<float>* ballPoint = dynamic_cast<myproj::Ball<float>*>(obj1);
            ballPoint->setVelocity(this->newVelocityObj1);
        }

        template <typename T, typename N>
        void Collision<T,N>::passVelocityObj2()
        {
            myproj::Ball<float>* ballPoint = dynamic_cast<myproj::Ball<float>*>(obj2);
            ballPoint->setVelocity(this->newVelocityObj2);
        }
        template <typename T, typename N>
        bool Collision<T,N>::obj1Ball()
        {
            return dynamic_cast<myproj::Ball<float>*>(obj1);
        }

        template <typename T, typename N>
        bool Collision<T,N>::obj2Ball()
        {
            return dynamic_cast<myproj::Ball<float>*>(obj2);
        }

        template <typename T, typename N>
        bool Collision<T,N>::obj1Wall()
        {
            return dynamic_cast<myproj::Plane<float>*>(obj1);
        }

        template <typename T, typename N>
        bool Collision<T,N>::obj2Wall()
        {
            return dynamic_cast<myproj::Plane<float>*>(obj2);
        }

        template <typename T, typename N>
        bool Collision<T,N>::ballWall() const
        {
            return dynamic_cast<myproj::Ball<float>*>(obj1) && dynamic_cast<myproj::Plane<float>*>(obj2)
                    || dynamic_cast<myproj::Plane<float>*>(obj1) && dynamic_cast<myproj::Ball<float>*>(obj2);
        }

        template <typename T, typename N>
        bool Collision<T,N>::ballBall() const
        {
            return dynamic_cast<myproj::Ball<float>*>(obj1) && dynamic_cast<myproj::Ball<float>*>(obj2);
        }

        template <typename T, typename N>
        void Collision<T,N>::handle(double x)
        {
            if (this->ballBall())
            {
               // myproj::Ball<float>* ball1 = dynamic_cast<myproj::Ball<float>*>(this->obj1);
                //myproj::Ball<float>* ball2 = dynamic_cast<myproj::Ball<float>*>(this->obj2);



            }
            else if (this->ballWall())
            {

            }
        }

}

#endif
