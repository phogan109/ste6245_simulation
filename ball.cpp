
//#include "ball.h"

#ifdef BALL

namespace myproj
{

    //DEFUNCT
    template <typename T>
    void Ball<T>::collisionSimulate(double dt)
    {
        this->localSimulate(dt);
    }

    //SETTER FOR MASS
    template <typename T>
    void Ball<T>::setMass(T m)
    {
        this->mass = m;
    }

    //SETTER FOR VELOCITY
    template <typename T>
    void Ball<T>::setVelocity(GMlib::Vector<T, 3> v)
    {
        this->velocity = v;
    }

    //SETTER FOR FLOOR
    template <typename T>
    void Ball<T>::setFloor(GMlib::PSurf<T, 3>* floor)
    {
        this->floor = floor;
    }

    //GETTER FOR MASS
    template <typename T>
    T Ball<T>::getMass()
    {
        return this->mass;
    }

    //GETTER FOR VELOCITY
    template <typename T>
    GMlib::Vector<T, 3> Ball<T>::getVelocity()
    {
        return this->velocity;
    }

    //GETTER FOR FLOOR
    template <typename T>
    GMlib::PSurf<T, 3>* Ball<T>::getFloor()
    {
        return this->floor;
    }

    //GETTER FOR U
    template <typename T>
    T Ball<T>::getU()
    {
        return this->_u;
    }

    //GETTER FOR V
    template <typename T>
    T Ball<T>::getV()
    {
        return this->_v;
    }

    //FUNCTION TO UPDATE DS BASED ON DT PASSED IN
    template <typename T>
    void Ball<T>::updateStep(double dt)
    {
        GMlib::DMatrix<GMlib::Vector<T,3> > m;
        GMlib::Vector<T,3> newpos;
        GMlib::Vector<T,3> step;
        GMlib::Point<T,3> p;

        step = (dt * this->velocity) + (.5 * std::pow(dt, 2) * g);
        p = this->getPos() + step;
        this->floor->getClosestPoint(p, this->_u, this->_v);
        m = this->floor->evaluate(this->_u, this->_v, 1, 1);
        n = m[0][1]^m[1][0];
        newpos = m[0][0] + this->getRadius()*n;

        this->ds = newpos - this->getPos();
        this->velocity += dt*this->g;
        this->velocity -= (this->velocity*n)*n;

    }

    //SETTER FOR DS
    template <typename T>
    void Ball<T>::setDS(GMlib::Vector<T, 3> newDS)
    {
        this->ds = newDS;
    }

    //GETTER FOR DS
    template <typename T>
    GMlib::Vector<T,3> Ball<T>::getDS()
    {
        return this->ds;
    }

    //SETTER FOR ROTATION VECTOR
    template <typename T>
    void Ball<T>::setRotationVector(GMlib::UnitVector<T, 3> newRotation)
    {
        this->rotation = newRotation;
    }

    //GETTER FOR ROTATION VECTOR
    template <typename T>
    GMlib::UnitVector<T,3> Ball<T>::getRotationVector()
    {
        return this->rotation;
    }

    //SETTER FOR XDS
    template <typename T>
    void Ball<T>::setXDS(T newXDS)
    {
        this->xDS = newXDS;
    }

    //GETTER FOR XDS
    template <typename T>
    T Ball<T>::getXDS()
    {
        return this->xDS;
    }


}

#endif

