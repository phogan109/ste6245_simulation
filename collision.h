#ifndef COLLISION
#define COLLISION

#include <memory>
#include <gmCoreModule>
#include "ball.h"
#include "plane.h"

namespace myproj
{

/***************************DEFUNCT **********************************/


template <typename T, typename N>
class Collision
{
   public:
    Collision();
    Collision(T* obj1, N* obj2, double dt);
    ~Collision();

    T* getObj1() const;
    N* getObj2() const;
    double getDt() const;
    bool getInit() const;

    void localSimulateObj1();
    void localSimulateObj2();

    bool obj1Ball();
    bool obj2Ball();
    bool obj1Wall();
    bool obj2Wall();
    bool ballWall() const;
    bool ballBall() const;

    void handle(double x);

    void setVelocityObj1(GMlib::Vector<float,3>);
    void setVelocityObj2(GMlib::Vector<float,3>);

    void passVelocityObj1();
    void passVelocityObj2();

    operator < (const Collision&);
    operator == (const Collision&);

   private:
    T* obj1;
    N* obj2;
    double dt;
    GMlib::Vector<float,3> newVelocityObj1;
    GMlib::Vector<float,3> newVelocityObj2;

    bool init {0};

};

}

#include "collision.cpp"



#endif // COLLISION

