#ifndef ASTEROID
#define ASTEROID

#include <parametrics/gmpasteroidalsphere>

namespace myproj
{

template <typename T>
class Asteroid : public GMlib::PAsteroidalSphere<T>
{
    GM_SCENEOBJECT(Asteroid)
    public:
     using GMlib::PAsteroidalSphere<T>::PAsteroidalSphere;

    protected:
     void localSimulate(double dt) override {

         //this->translate( dt * velocity );
         //this->rotate( dt * rotation, velocity  );

    }

    private:
     bool m_test01 {false};
     GMlib::Vector<T, 3> velocity { GMlib::Vector<float,3>( -1.0f, -1.0f, -1.0f ) };
     T mass;
     GMlib::Angle rotation {90};
};

}

#endif // ASTEROID

